#!/bin/bash

while true
do
    myw=$(xdotool getactivewindow)
    mywinprop=$(xprop -id ${myw})
    if [[ \
        $(echo "$mywinprop" | egrep 'program specified minimum size: 430 by 158') && \
        $(echo "$mywinprop" | egrep 'WM_CLASS.*Subl3') \
        ]]
    then
        echo ok
        xvkbd -text "\[Alt]\[F4]" >/dev/null 2>&1
    else
        sleep 0.5
    fi
done
