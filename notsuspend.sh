#!/bin/bash

# Grab current sleep timeout on battery and ac
a=$(xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/inactivity-on-ac)
b=$(xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/inactivity-on-battery)

# Set sleep to never on battery and ac
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/inactivity-on-ac -s 0
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/inactivity-on-battery -s 0

# Your task here
sleep 5

# Reset sleep to what it was before on battery and ac
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/inactivity-on-ac -s $a
xfconf-query -c xfce4-power-manager -p /xfce4-power-manager/inactivity-on-battery -s $b