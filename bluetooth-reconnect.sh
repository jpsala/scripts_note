#!/bin/expect
set address "FC:58:FA:AB:9E:34"
spawn sudo bluetoothctl -a KeyboardOnly
expect "#"
send "power on\n"
expect "succeeded"
#interact
send "connect $address\n"
expect {
  "Connection successful" {
    send_user "\n*******\nconectado\n*******\n"
    timeout {
      close
      spawn ./reconnect.sh
    }
    exit
  }
  "not available" {
    send "scan on\n"
    expect {
      "Bluetune-Bean" {
        send "connect $address\n"
        expect "Connection successful"
        send_user "\n*******\nconectado\n*******\n"
      }
      timeout {
        send "adfasdfasdf"
        send_user "\n********\nerror escaneando\n*************\n"
        exit
      }
    }
  }
}