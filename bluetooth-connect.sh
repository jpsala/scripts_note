#!/bin/expect
set address "FC:58:FA:AB:9E:34"
spawn sudo bluetoothctl -a KeyboardOnly
expect "#"
send "power on\n"
expect "succeeded"
#interact
send "connect $address\n"
expect {
  "Connection successful" {
    send_user "\n*******\nconectado\n*******\n"
    exit
    
  } "Failed" {
      send_user "\nFailed\n"
      exp_continue
  }
}
send "remove $address\r"
expect "removed"
expect -re $prompt
send "scan on\r"
expect {
  timeout {
    send "scan off\n"
    expect "no"
    send "quit\n"
    abort
  }
}
expect "Bluetune-Bean"
send "scan off\r"
send "trust $address\r"
expect "succeeded"
send "pair $address\r"
expect "Paired: yes"
send_user "\nShould be paired now.\r"
send "connect $address\r"
expect "Connection successful"
send "quit\r"
expect eof