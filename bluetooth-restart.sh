#!/bin/expect
set prompt "#"
#set address [lindex $argv 0]
set address "FC:58:FA:AB:9E:34"
spawn sudo bluetoothctl -a KeyboardOnly
expect -re $prompt
send "remove $address\r"
expect -re $prompt
send "scan on\r"
expect -re "Bluetune-Bean"
send "scan off\r"
expect "Controller"
send "trust $address\r"
expect -r "succeeded"
send "pair $address\r"
expect -r "Pairing successful"
send "connect $address\r"
expect -re "Connection successful"
send_user  "\nDebería de haber conectado!!!\n"
send "quit\r"
expect eof